from typing import List
from django.contrib import admin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls.resolvers import URLPattern
from .models import Question, Choice
from django.urls import reverse
from django.utils.html import format_html
from django.conf.urls import path
import csv

def export_to_csv(modeladmin, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export.csv"'

    writer = csv.writer(response)
    model_fields = queryset.model._meta.fields
    header_row = [field.name for field in model_fields]
    writer.writerow(header_row)

    for obj in queryset:
        data_row = [str(getattr(obj, field.name)) for field in model_fields]
        writer.writerow(data_row)

    return response
export_to_csv.short_description = "Export ausgewählter Objekte als CSV"


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {"fields": ["question_text"]}),
        ("Date information", {"fields": ["pub_date"], "classes": ["collapse"]}),
    ]
    inlines = [ChoiceInline]
    list_display = ["question_text", "pub_date", "was_published_recently"]
    list_filter = ["pub_date"]
    search_fields = ["question_text"]
    actions = [export_to_csv]



class ChoiceAdmin(admin.ModelAdmin):
    list_display = ["choice_text", "votes", "tour_actions"]
    actions = [export_to_csv]

    def get_urls(self):
        urls = super().get_urls()

        custom_urls = [
            path(
                "(<choice_id>)/generate/",
                self.admin_site.admin_view(self.vote_choice),
                name="vote_choice",
            ),
        ]
        return custom_urls + urls

    def tour_actions(self, obj):
        #return "Hello World"
        return format_html('<a class="related-widget-wrapper-link button" href="{}" id="vote">Vote</a>',
                           reverse("admin:vote_choice", args=[obj.pk], current_app="polls"))
    
    def vote_choice(self, request, choice_id):
        selected_choice = Choice.objects.get(pk=choice_id)
        selected_choice.votes += 1
        selected_choice.save()
        return redirect("/admin/polls/choice/"+ choice_id)



admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)

